//
//  Group.swift
//  Notes
//
//  Created by Filip Klembara on 10/04/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation

public struct Group {
    public let name: String
    public let notes: [Note]

    public init(name: String, notes: [Note] = []) {
        self.name = name
        self.notes = notes
    }
}
