//
//  Note.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation

public struct Note {
    public let date: Date
    public let text: String
}
