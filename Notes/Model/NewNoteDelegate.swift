//
//  NewNoteDelegate.swift
//  Notes
//
//  Created by Filip Klembara on 11/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

public protocol NewNoteDelegate: AnyObject {
    func saveNew(note: String)
}
