//
//  NewNoteViewController.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import UIKit

class NewNoteViewController: UIViewController {

    weak var delegate: NewNoteDelegate?

    @IBOutlet weak var newNote: UITextField!
    
    @IBAction func saveNote(_ sender: UIBarButtonItem) {
        guard let text = newNote.text, text != "" else {
            let av = UIAlertController(title: "Error", message: "Insert text", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            av.addAction(action)
            present(av, animated: true, completion: nil)
            return
        }
        delegate?.saveNew(note: text)
        navigationController?.popViewController(animated: true)
    }
}
