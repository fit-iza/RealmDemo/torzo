//
//  NoteModel.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation
import UIKit

public class NoteModel: NSObject {
    init(group: Group) {
        notes = group.notes
    }

    private var notes: [Note]

    public var count: Int {
        return notes.count
    }
    
    public subscript(index: Int) -> Note {
        return notes[index]
    }
    
    public func insert(note: Note) {
        notes.insert(note, at: notes.startIndex)
    }
    
    public func remove(at index: Int) {
        notes.remove(at: index)
    }
}
